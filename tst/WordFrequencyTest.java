import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import org.junit.jupiter.api.Test;

class WordFrequencyTest {
    private final WordFrequency obj = new WordFrequency();

    private void expectEmptyResponse(String block) {
        List<String> result = obj.getMostCommonWords(block);
        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    private void expectOneResponse(String block, String expected) {
        List<String> result = obj.getMostCommonWords(block);
        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals(expected, result.get(0));
    }

    private void expectMultipleResponse(String block, String[] expected) {
        List<String> result = obj.getMostCommonWords(block);
        assertNotNull(result);
        assertEquals(expected.length, result.size());
        // Could use Hamcrest here, but the challenge is to not use third party packages.
        // TODO: Finish verifying list
    }

    @Test
    void wordFrequencyNullInputTest() {
        expectEmptyResponse(null);
    }

    @Test
    void wordFrequencyEmptyStringTest() {
        expectEmptyResponse("");
    }

    @Test
    void wordFrequencySpaceTest() {
        expectEmptyResponse(" ");
    }

    @Test
    void wordFrequencyTabTest() {
        expectEmptyResponse("\t");
    }

    @Test
    void wordFrequencyNewLineTest() {
        expectEmptyResponse("\n");
    }

    // Could test more whitespace characters if deemed necessary
    // https://docs.oracle.com/javase/7/docs/api/java/lang/Character.html#isWhitespace(char)

    @Test
    void wordFrequencyOneWordTest() {
        expectOneResponse("word", "word");
    }

    @Test
    void wordFrequencySameWordTest() {
        expectOneResponse("word\nword\tword", "word");
    }

    @Test
    void wordFrequencyMixedWordsOneResponseTest() {
        expectOneResponse("word\twhat\nword", "word");
    }

    @Test
    void wordFrequencyMixedWordsTwoResponsesTest() {
        expectMultipleResponse("word\twhat\nword what", new String[]{"word", "what"});
    }

    @Test
    void wordFrequencyThreeWordsTwoResponsesTest() {
        expectMultipleResponse("word\twhat another\nword what", new String[]{"word", "what"});
    }

    @Test
    void wordFrequencyMixedCapitalsTest() {
        expectOneResponse("\tWhat another\nword WHAT", "what");
    }

    @Test
    void wordFrequencyRandomSymbolsTest() {
        expectOneResponse("\twhat#2 %another\nword $$what,", "what");
    }

    @Test
    void wordFrequencyRandomSymbolsBetweenWordsTest() {
        expectOneResponse("\tWh#at2 %another\nanother@ $WHA$T,", "another");
    }
}
