import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class WordFrequency {

    /**
     * Time and space complexity: This solution is optimized for time
     * over space. The code iterates over all characters in the block,
     * delimiting words on anything that isn't a letter. Once a word is
     * considered to be terminated, it is then stored in a HashMap. The
     * map indicates the frequency of each word observed. Since insertion
     * and access is O(1) (amortized over changing the size of the map
     * on occasion), the cost of checking and then incrementing a word's
     * frequency is proportional to the size of the average word observed
     * divided by the number of characters in the block. Reusing the
     * StringBuilder to aggregate each of the words observed is also
     * an operation over every character. There's also the time spent
     * adding words to the result and clearing the result upon finding a
     * new maximum.
     * TIME:
     * Let n be the number of characters in block.
     * Let m be the average size of each word in block.
     * - Ignoring non-letter characters - O(n)
     * - Adding each letter to the StringBuilder - O(n)
     * - Accessing observed words - O(m / n)
     * - Incrementation of observed words - O(m / n)
     * - Insertion of observed words - O(m / n)
     * - Adding new word to result - O(m / n)
     * - Clearing result list on new maximum - O(m / n)
     * - Clearing the StringBuilder - O(n)
     * Total - 3 * C * O(n) + 5 * D * O(m / n) = O(n)
     *
     * SPACE:
     * - wordToFrequency - O(n)
     * - builder - O(m)
     * - maxFrequency - O(1)
     * - word - O(m)
     * - freq - O(1)
     * - result - O(n)
     * Total - 2 * C * O(n) + 2 * D * O(m) + 2 * E * O(1) = O(n)
     *
     * Considerations and limitations: The first limitation is that
     * both Character.isLetter() and Character.toLowerCase() depend
     * on each character in block not being a supplementary character.
     * In order to support supplementary characters, both methods
     * could be substituted to use their counterpart methods that take
     * in an int instead, which is referencing the character's Unicode
     * code point.
     * The second limitation is that this method cannot handle a block
     * that has a length of Integer.MAX_VALUE. It would be trivial to
     * change the method in order to do the necessary operations after
     * original block ends. For the sake of code simplicity, I decided
     * to simply do the same logic by considering a space at the end of
     * iterating through block.
     * The third limitation is that this implementation doesn't consider
     * hyphenated words or contractions. I did leave an inline note in
     * the code where this behavior could be changed. Since the specs
     * did not clarify what counted for valid words, I just assumed the
     * simplest specifications.
     */
    List<String> getMostCommonWords(String block) {
        List<String> result = new ArrayList<>();
        if (null == block || block.equals("")) {
            return result;
        }

        Map<String, Integer> wordToFrequency = new HashMap<>();
        StringBuilder builder = new StringBuilder("");
        int maxFrequency = 0;

        for (int i = 0; i <= block.length(); i++) {
            char c = (i < block.length()) ? block.charAt(i) : ' ';
            if (Character.isLetter(c)) { // could change this to include '\'' for differentiating between what and what's
                builder.append(Character.toLowerCase(c));
                continue;
            } else if (builder.toString().isEmpty()) {
                continue;
            }
            // Word is broken
            String word = builder.toString();
            builder.setLength(0);
            int freq = wordToFrequency.getOrDefault(word, 0);
            wordToFrequency.put(word, ++freq);
            if (freq < maxFrequency) {
                continue;
            } else if (freq > maxFrequency) {
                maxFrequency = freq;
                result.clear();
            }
            result.add(word);
        }
        return result;
    }
}
